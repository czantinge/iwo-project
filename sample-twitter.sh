##!/bin/bash
#a shell script that returns some values from the twitter corpus
#courtjen zantinge s3047393

#how many tweets on march 1 2017
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l
#how many unique tweets
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | uniq -u | wc -l

#how many retweets out of the unique tweets


